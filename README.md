****ArXiv Recommender****

The goal of this semestral project was to a create an ArXiv Recommender system that enables to search in very dense corpus of abstracts of ArXiv documents and recommend the most suitable one based on entered query.

*Data for this work are available here:* https://www.kaggle.com/Cornell-University/arxiv

*These are the data features:*
* id: ArXiv ID (can be used to access the paper, see below)
* submitter: Who submitted the paper
* authors: Authors of the paper
* title: Title of the paper
* comments: Additional info, such as number of pages and figures
* journal-ref: Information about the journal the paper was published in
* doi: [https://www.doi.org](Digital Object Identifier)
* abstract: The abstract of the paper
* categories: Categories / tags in the ArXiv system
* versions: A version history


*Code for ArXiv recommender work:* NI-MVI_ArXiv.ipynb

*Milestone:* Milestone.pdf

*Report:* report.pdf

*Results:* 
* BERT-results
* Tfidf-results
* Word2Vec-results
* clustering_results